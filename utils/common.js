export function getTheStorage(key) {
				var notes;
				try{
					const value = uni.getStorageSync(key)
					if(value){     // 如果缓存没有，取出来的是空白，不是null
						notes = value
					}
					else{
						notes = []
					}
				}catch(e){
					notes = []
				}
				return notes;
			}
