# 云开发备忘录
## 项目准备工作
  1. 注册登录Hbuilder X账号

     ![image-20231108153650086](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108153650086.png)

  2. 创建uniCloud项目（选择默认模板，启用uniCloud，阿里云）

  3. 在项目目录右键uniCloud，选择关联云服务空间或项目——新建申请服务空间——关联选定的云空间,关联成功如下图所示：![image-20231108153950567](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108153950567.png)

  4. 在云数据库中添加数据记录
     右键uniCloud目录打开控制台，在云数据库中创建数据表notes，并添加记录，填写完title、content、time、status等内容后，会自动生成“记录ID”字段，显示为_id

  5. 在cloudfunctions文件夹下新建云函数，函数名为“CloudGetData",创建完后在文件夹下生成对应的CloudGetData子目录，下面有index.js和package.json两个文件

     ![image-20231108191635349](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108191635349.png)

  6. 修改index.js中的代码，客户传值包括type和id，用event对象保存，判断type的值是否为all，当为all时，返回备忘录的全部值，此时id无意义，当type的值不为all时，只获取_id=客户传入参数id的那一条记录

     	let { type, id} = event
          	let res;
          	if (type == 'all'){
          		res = await db.collection("notes").orderBy(
          	                                  "time","desc").get();
          	}else{
          		res = await db.collection("notes").doc(id).get()
          	}

     7. 将图片考到static/images目录下，将原utils文件夹（含tools.is和commons.js)拷贝到当前项目中

        ![image-20231108191803847](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108191803847.png)![image-20231108191816519](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108191816519.png)

     8. 删除默认页面index，包括删除pages目录下的index文件夹以及index.vue文件，并删除pages.json里面pages[]中关于index路径说明的内容
## 项目开始
### list页面修改
1. 创建list页面

2. 在pages.json注册

3. 拷贝上次代码，在此基础上做修改

4. 在插件安装市场安装页面中用到的uni插件，安装到uni_modules目录下

   ![image-20231108192019947](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108192019947.png)

5. 原来是缓存getTheStorage中获取数据，现在是要从云数据库中获得，调用云函数：uniCloud.callFunction,在data中传入两个值type和id，并设置type的值为all，要求返回全部备忘录的值，返回后的值会放在data里面，然后将保存在items[]数组中。
   getData() {
				uniCloud.callFunction({
					name: "CloudGetData",
					data: {
						type:'all',   //要求返回全部备忘录
						id:"000"      //当type为'all'时候，id无意义
					}
				}).then(res => {
					console.log(res)
   this.items = res.result.data

   
### detail页面修改
1. 新建detail页面，并在pages.json中注册

   ![image-20231108193041536](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108193041536.png)

2. 拷贝源代码，以此为基础做修改

3. 修改noteItem的值，新增加id属性，为了与云数据库中记录_id，获取两者相等的那条记录

  ![image-20231108193125855](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108193125855.png)

4. 原来取一条数据是从缓存中，现在修改为从云函数中获取，并且设置type为one，要求获取一条备忘录的值，传给云函数的参数id为选中的那条记录的id

   getTheItemFromCloud() {
   				uniCloud.callFunction({
   					name: "CloudGetData", //指明你将要调用的云函数的名称
   					data: {               //输入参数
   						type: 'one',   
   						id: this.noteId    //获取一条记录以及该记录的id值
   					}

5. 修改detail页面加载时的onLoad方法，让页面加载时，不从缓存中获取数据，而是调用云函数即调用第4步的getTheItemFromCloud（），将点击card的那条记录的id传递到detail页面来

   ![image-20231108193448358](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108193448358.png)

6. 修改list页面，页面跳转url携带那条记录的id，存到noteId中，将其在调用云函数时，作为参数id传给云函数，去与_id匹配

   ![image-20231108193603998](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108193603998.png)

### 更新数据

1. 添加一个云函数CloudUpdateData,用于更新数据

2. 修改index.js文件，判断type值为添加、更新、删除

   ![image-20231108193632663](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108193632663.png)

3. 添加和修改在forSubmit方法中，不需要从缓存中获取数据了，因为在此页面加载时，已经调用getTheItemFromCloud返回当前记录，写入缓存也改为写入云数据库。

   ![image-20231108193724331](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108193724331.png)

4. 删除方法中，不再从缓存中取出删除，而是在调用的setTheStorage方法中，CloudUpdateData方法中，有删除数据语句。
    delNote() {

  			uni.showModal({
  				content: "确定删除？",
  				success: (res) => {
  					if (res.confirm) {this.setTheStorage('delete', "删除成功！")}}}）

### 运行图如下
+ deatil添加数据

  ![image-20231108195104730](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108195104730.png)
  
+ 添加成功提示

  ![image-20231108195251099](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108195251099.png)

+ list页面显示

  ![image-20231108195310015](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108195310015.png)

+ 记录状态更改

  ![image-20231108195339262](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108195339262.png)![image-20231108195407396](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108195407396.png)

+ 删除记录

  ![image-20231108195428105](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108195428105.png)

+  删除成功

![image-20231108195453363](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108195541819.png)![image-20231108195617843](D:\markdown\项目\云开发备忘录cloud.assets\image-20231108195617843.png)
